(function ($) {
	/*SLICK*/
jQuery(document).ready(function($) {
	$('.slider').slick({
		dots: true,
		speed:1000,
		arrows:false,
		autoplay:false,
		autoplaySpeed:500,
	});
});
	/*FOR MENU*/
$('.burger').click(function(){
	$(this)
	.find('.icon')
	.toggleClass('menu')
	.toggleClass('close');

	$('#menu').toggleClass('open');
	$('body').toggleClass('menu_open');
});
	/*VALIDATE*/
$(function() {

		$("#js-form").validate({

				rules: {
					form_name: {
						required:true
					},
					phone:{
						required:true,
						digits:true
					}
				},
				messages:{
					form_name: "Введите имя",
					phone:"Введите номер телефона"
				}
		});
});
	/*MODAL*/
var modal=$(".js-modal"),
		btnModal = $(".js-btn-modal"),
		btnCloseModal = $(".js-close-modal");

		btnModal.on('click', function(event) {
			event.preventDefault();
			modal.addClass('open');
		});

		btnCloseModal.on('click', function(event) {
			event.preventDefault();
			modal.removeClass('open');
		});

		/*----------------------------------------
	 VALIDATIONS JQUERY FORM
	 ----------------------------------------*/
	$(function () {
		/* RULES */
		$("#js-form2").validate({
			focusCleanup: true,
			focusInvalid: false
		});
	});

	/*MASK*/
	$('input[type="tel"]').mask('+7(000)-000-00-00', {
		selectOnFocus: true,
		placeholder: "+7 (___) ___-__-__"
	});

	/*----------------------------------------
	 TRANSITION SCROLL
	 ----------------------------------------*/
	$('.scroll').on("click", function (e) {
		// e.preventDefault();
		var anchor = $(this);
		$('html, body').stop().animate({
			scrollTop: $(anchor.attr('href')).offset().top
		}, 1000);
	});
})(jQuery);

